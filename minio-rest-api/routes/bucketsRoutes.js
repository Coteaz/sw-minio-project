var express = require('express'),
    multer = require("multer"),
    path = require('path');

function setup(baseRouter, handlers) {

  // ===========================================================================
  // BUCKET ROUTER
  // ===========================================================================
  var router = express.Router({mergeParams: true});

  router.route('/')
    .get(handlers.getBuckets);

  router.route('/')
    .post(handlers.createBucket);

  router.route('/:bucketName')
    .delete(handlers.deleteBucket);

  router.route('/:bucketName/')
    .get(handlers.getBucketObjects);

  router.route('/:bucketName/')
    .post(multer({
      // Use memory as storage instead of the file system
      storage: multer.memoryStorage(),

      // Let's just accept images
      fileFilter: function (req, file, cb) {
          // Check extention
          if (['.png', '.gif', '.jpg', '.jpeg'].indexOf(path.extname(file.originalname)) === -1) {
              return cb({
                name: 'UploadError',
                code: 'ExtensionNotSupported',
                message: 'Only "png", "gif", "jpg" and "jpeg" extensions are allowed.'
              }, false);
          }

          // Check mime type
          if (['image/gif', 'image/jpeg', 'image/png', 'image/pjpeg', 'image/x-png'].indexOf(file.mimetype) === -1) {
              return cb({
                name: 'UploadError',
                code: 'MimetypeNotSupported',
                message: 'Only "image/gif", "image/jpeg", "image/png", "image/pjpeg" and "image/x-png" mime type are allowed.'
              }, false);
          }

          // TODO: Magic number

          cb(null, true)
      }
    }).single("file"), handlers.uploadBucketObject);

  router.route('/:bucketName/:fileName')
    .get(handlers.getBucketObject);

  router.route('/:bucketName/:fileName')
    .delete(handlers.deleteBucketObject);

  /*
  ROUTES REGISTER
  =============================================================================
  */
  baseRouter.use('/buckets/', router);
}

exports.setup = setup;
