// =============================================================================
// DEPENDENCIES
// =============================================================================
var express     = require('express');
var app         = express();

var http        = require('http')
var bodyParser  = require('body-parser');

// Configure app to use bodyParser(), this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// =============================================================================
// API ROUTES
// =============================================================================
var router = express.Router();

router.get('/', function(req, res) {
    res.json({
      api: 'minio-rest-api',
      timestamp: Math.floor(Date.now() / 1000)});
});

app.use('/api', router);

// =============================================================================
// REGISTER ROUTES
// =============================================================================
var config = {
    /*
     * Load routes and handlers dependencies
     */
    dependencies: {
        buckets: {
            routes: './routes/bucketsRoutes',
            handlers: './handlers/bucketsHandlers'
        }
    }
};

for (dependency in config.dependencies) {
    require(config.dependencies[dependency].routes).setup(router, require(config.dependencies[dependency].handlers));
}

// =============================================================================
// ERRORS HANDLERS
// =============================================================================
app.use(function errorHandler(err, req, res, next) {
    // Log the stack trace internaly.
    if (req.app.get('env') === 'development') {
      console.error(err.stack || err.toString());
    }

    res.status(err.statusCode || 500).json({name: err.name, code: err.code, message: err.message});
});

// =============================================================================
// START HTTP SERVER
// =============================================================================

var port = process.env.PORT || 8080;

var server = http.createServer(app);

var listening = server.listen(port, function () {
  console.log('Listening on: http://localhost:' + listening.address().port + '/api');
});
