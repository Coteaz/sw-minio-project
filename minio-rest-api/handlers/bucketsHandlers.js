// Load required models
var Minio = require('minio'),
    util = require('util');

var s3Client = new Minio.Client({
  endPoint: process.env.MINIO_ENDPOINT,
  port: parseInt(process.env.MINIO_PORT),
  secure: (process.env.MINIO_SECURE === 'true' ? true : false),
  region: process.env.MINIO_REGION,
  accessKey: process.env.MINIO_ACCESS_KEY,
  secretKey: process.env.MINIO_SECRET_KEY
});

exports.getBuckets = function (req, res, next) {
  s3Client.listBuckets(function(e, buckets) {
    if (e) return (next(e))

    return (res.status(200).json({buckets: buckets}));
  });
};

exports.createBucket = function (req, res, next) {
  var bucketName = req.body.bucketName;

  s3Client.makeBucket(bucketName, process.env.MINIO_REGION, function(e) {
    if (e) return (next(e))

    // Set default policy to r/w to allow browsering of files directly
    // from minio server
    s3Client.setBucketPolicy(bucketName, '*', Minio.Policy.READWRITE, function(e) {
      if (e) return (next(e))

      return (res.status(200).json({message: util.format('Success, bucket (r/w) "%s" created!', bucketName)}));
    })
  })
};

exports.deleteBucket = function (req, res, next) {
  var bucketName = req.params.bucketName;

  s3Client.removeBucket(bucketName, function(e) {
    if (e) return (next(e))

    return (res.status(200).json({message: util.format('Success, bucket "%s" deleted!', bucketName)}));
  })
};

exports.getBucketObjects = function (req, res, next) {
  var bucketName = req.params.bucketName;

  var objectsStream = s3Client.listObjectsV2(bucketName, '', true)

  var files = [];

  objectsStream.on('data', function(obj) {
    var publicUrl = s3Client.protocol + '//' + 'localhost' + ':' + s3Client.port + '/' + bucketName + '/' + obj.name
    obj['link'] = publicUrl;

    files.push(obj);
  });

  objectsStream.on('error', function(e) {
    if (e) return (next(e))
  });

  objectsStream.on('end', function(e) {
    return (res.status(200).json({files: files}));
  });
};

exports.uploadBucketObject = function (req, res, next) {
  var bucketName = req.params.bucketName;

  s3Client.putObject(bucketName, req.file.originalname, req.file.buffer, function(e, etag) {
    if (e) return (next(e))

    return (res.status(200).json({message: util.format('Success, file uploaded on bucket "%s" !', bucketName)}));
  });
};

exports.getBucketObject = function (req, res, next) {
  return (res.status(501).json({message: 'Not Implemented - getObject'}));
};

exports.deleteBucketObject = function (req, res, next) {
  return (res.status(501).json({message: 'Not Implemented - deleteObject'}));
};
