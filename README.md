# sw-minio-project

## Requirements

- [Docker](https://docs.docker.com/engine/installation/)
- [docker-compose](https://docs.docker.com/compose/install/)

## Usage

```bash
# Build image
docker-compose build

# Run in background
docker-compose up -d

# Run
docker-compose up

# Stops containers and removes containers, networks
docker-compose down
```

The API is available on `http://localhost:8080/api`
