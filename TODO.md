# TODO

## Bug

* (¿ right beheviour ?) **deleteBucket()**: The bucket you tried to delete is not empty

## Endpoints

* Delete file
* Get single file information
* Specific `/api/objects/` endpoints

## Misc

* [ApiDoc](http://apidocjs.com/)
* Test ([Mocha](https://github.com/mochajs/mocha) / [Chai](https://github.com/chaijs/chai))
* Real logging
* (?) NGinx proxy mode -> Minio server (serve file)
* Consider Cache
* Validate MINIO env variables
* (?) Validate parameters
* (?) Custom responses
* Support filtering, sorting, and pagination on collections
* (?) Naming
  * buckets -> albums
  * objects -> images
* Refactor loading of routes / handlers.
* Gather `package-lock.json` file after build
